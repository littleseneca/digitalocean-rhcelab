terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}
# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = file("token.vault")
}
resource "digitalocean_ssh_key" "localkey" {
  name = "jenny"
  public_key = file("~/.ssh/id_rsa.pub")
}