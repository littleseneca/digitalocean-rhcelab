resource "digitalocean_volume" "volumes" {
  count       = 4
  region      = var.local
  name        = "drive${count.index}"
  size        = 1
  description = "an example volume"
}
