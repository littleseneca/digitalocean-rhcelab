
variable "image" {
  default = "centos-stream-8-x64"
}
variable "size" {
  default = "s-1vcpu-1gb"
}
variable "local" {
  default = "sfo3"  
}
variable "controller-tags" {
  type    = list(string)
  default = ["rhce","controller"]  
}
variable "worker-tags" {
  type    = list(string)
  default = ["rhce","worker"]  
}
