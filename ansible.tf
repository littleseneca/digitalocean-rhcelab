resource "null_resource" "rhce-lab" {
    provisioner "remote-exec" {
        connection {
            host = digitalocean_droplet.controller.ipv4_address
            user = "root"
            type = "ssh"
            private_key = file("~/.ssh/id_rsa")
            timeout = "2m"
        }
        inline = [
            "echo Connected!!!"
        ]
    }
    provisioner "local-exec" {
        command = "ansible-playbook provisioner.yml"
    }
    depends_on = [digitalocean_droplet.node, digitalocean_droplet.controller, digitalocean_volume_attachment.volumeattch]
}