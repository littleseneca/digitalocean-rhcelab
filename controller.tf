resource "digitalocean_droplet" "controller" {
  image     = var.image
  name      = "controller.example.com"
  region    = var.local
  size      = var.size
  ssh_keys    = [digitalocean_ssh_key.localkey.id]
  tags      = var.controller-tags
}
output "IPAddress" {
  value = digitalocean_droplet.controller.ipv4_address
}