# digitalocean-rhcelab
## Purpose
This cool little tool uses Terraform to spin up 5 VMs for use as a lab for studying the material on the RHCE <= 8 exams. The lab consists of 1 controller and 4 workers, one of which has a second attached hard drive.
Once spun up, ansible is dynamically called from your local machine to provision the 5 VMs for basic networking, ansible installation on the controller, and a usable administrator login account for the 4 worker nodes, accessable only from the controller.

## Getting started
This tool assumes that you alread have terraform and ansible installed on your local device. 

1. Start by retrieving this repo:
~~~
git clone https://gitlab.com/littleseneca/digitalocean-rhcelab.git
~~~
2. Modify the file "token.tf", and add the following contents to it:
~~~
variable "key" {
  type    = list(string)
  default = ["your public keys from digital ocean you want to use to log into your Red Hat lab"]
}
~~~
3. Then, create the token.vault file, adding your digital ocean token as the ONLY contents of the file:

4. Once those actions are done built, run the following commands:
~~~
terraform init
~~~
~~~
terraform plan
~~~
~~~
terraform apply
~~~

Once those commands have been completed (the apply will take some time to finalize), you will have a fully provisioned RHCE homelab environment. 

To access your lab, use key based ssh to connect to the public IP address of your rhce-controller device. Then, use the username "administrator", and password "password" to access your other devices, named rhce-worker0, rhce-worker1, rhce-worker2, and rhce-worker3. the domain is example.com 

Security note:
The "administrator" account is ONLY accessable via password authentication, and can only be routed through rhce-controller, which can only be accessed through the root account with your assigned ssh-key. This is to stop script kiddies from connecting to your RHCE homelab devices.
