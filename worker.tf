resource "digitalocean_droplet" "node" {
  count       = 4
  image       = var.image
  name        = "node${count.index}.example.com"
  region      = var.local
  size        = var.size
  ssh_keys    = [digitalocean_ssh_key.localkey.id]
  tags        = var.worker-tags
}
resource "digitalocean_volume_attachment" "volumeattch" {
  count       = 4
  droplet_id  = digitalocean_droplet.node[count.index].id
  volume_id   = digitalocean_volume.volumes[count.index].id
}
